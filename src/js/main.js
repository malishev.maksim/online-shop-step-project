const products = [];
let filteredProducts = products;

const cart = {
  productQuantity: 0,
  products: [],
  quantities: [],//[{productID: 23123, qty: 4}]
  addProduct: function (productId) {
    if (!this.products.some(productInCart => productInCart.id == productId)) {
      let product = products.find(obj => obj.id == productId);
      cart.products.push(product);
      this.quantities.push({id: productId, quantity: 1})
    } else {
        this.quantities.find(obj => obj.id == productId).quantity++;     
    }
    document.querySelector("#cart-product-quantity").innerHTML = ++cart.productQuantity;
    console.log(cart);
  }
};

function buildRandomProduct() {
  return {
    id: Math.floor(Math.random() * 1000),
    name: ["Chair", "Big chair", "Small chair"][Math.floor(Math.random() * 3)],
    price: Math.floor(Math.random() * 5000),
    reviews: ["Good product"],
    oldPrice: 40.99,
    stars: 4,
    description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim, corporis.",
    stock: 5,
    category: ["Furniture", "Chairs", "Bedrooms"][Math.floor(Math.random() * 3)],
    tags: ["Big", "Chair"],
    size: ["S", "M", "L"][Math.floor(Math.random() * 3)],
    color: ["red", "black", "blue"][Math.floor(Math.random() * 3)],
    imgUrl: `./img/products/furniture/${Math.floor(Math.random() * 19 + 1)}.png`
  }
};

// inflate products array with dummy data:
let i = 1;
while (i < 10) {
  products.push(buildRandomProduct());
  i++;
};

console.log("TCL: products", products);

// generate html for each product in array

function buildProductHTML(product) {
  return `
    <div class="col-md-6 col-xl-4 d-flex justify-content-center">
    <div class="card card-product text-center mb-3" data-product-id = "${product.id}">
      <span class="card-product__quick-view-btn added-to-cart ${cart.products.some(cartProduct => cartProduct.id == product.id) ? "" : "hidden"}">
            <i class="fas fa-check-circle"></i>
            <span>In cart</span>
      </span>
      <div class="bg-for-img card-img-top">
        <div class="card-product__buttons-actions opaque d-flex px-3 justify-content-between">
          <a class="card-product__quick-view-btn" data-toggle="modal" data-target="#exampleModal" href="#">Quick
            view</a>
          <a class="card-product__quick-view-btn add-to-cart" href="#">
            <i class="fas fa-shopping-basket"></i>
            ${cart.products.some(cartProduct => cartProduct.id == product.id) ? "<span>In cart</span>" : "<span>Add to cart</span>"}
          </a>          
        </div>
        <img src="${product.imgUrl}" alt="furniture" class="product-img">
      </div>
      <div class="card-body">
        <h5 class="card-title">${product.name}</h5>
        <div class="stars">
          <i class="${product.stars >= 1 ? "fas" : "far"} fa-star"></i>
          <i class="${product.stars >= 2 ? "fas" : "far"} fa-star"></i>
          <i class="${product.stars >= 3 ? "fas" : "far"} fa-star"></i>
          <i class="${product.stars >= 4 ? "fas" : "far"} fa-star"></i>
          <i class="${product.stars >= 5 ? "fas" : "far"} fa-star"></i>
        </div>
        <div class="card-product__devider"></div>
        <div class="card-product__buttons-footer">
          <button class="card-product__icon-button">
            <i class="far fa-heart"></i>
          </button>
          <button class="card-product__icon-button">
            <i class="fas fa-compress-arrows-alt "></i>
          </button>
          <div class="btn-placeholder">
            <button type="button" class=" btn btn-primary">${product.price}</button>
          </div>
        </div>
      </div>
    </div>
    </div>`;
};

displayProducts(document.querySelector("#product-list-container"), products);

function displayProducts(container, productsArray) {
  container.innerHTML = "";
  for (let product of productsArray) {
    container.innerHTML += buildProductHTML(product);
  }
};
