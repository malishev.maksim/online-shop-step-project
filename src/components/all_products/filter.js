//initiate default filters state
const activeFilters = {
  categories: [],
  colors: [],
  sizes: [],
  tags: [],
  // priceMin: priceSlider.slider('getValue')[0],
  // priceMax: priceSlider.slider('getValue')[1]
  priceMin: 50,
  priceMax: 5000
};

// Listen changes in filters
$(".filter__checkbox").change(() => {
  updateActiveFilters();
  filterProducts(products, activeFilters);
  // updateProductQuantityForFilters();
});

$(".tag").click(function (e) {
  e.preventDefault();

  //Activate and deactivate tag by click
  if ($(this).attr("data-tag-active") === "true") {
    $(this).attr("data-tag-active", "false");
  } else {
    $(this).attr("data-tag-active", "true");
  };

  updateActiveFilters();
  filterProducts(products, activeFilters);
  // updateProductQuantityForFilters();
});

//show default quantity for filters
updateProductQuantityForFilters();

function showProductQuantityForFilter(products, $filterElement, $placeToShow) {

  switch ($filterElement.data("filter-type")) {
    case "category": {
      let productsForFilter = products.filter(product => $filterElement.attr("id") === product.category);
      console.log(productsForFilter);
      $placeToShow.html(productsForFilter.length);
      break;
    };
    case "size": {
      let productsForFilter = products.filter(product => $filterElement.attr("id") === product.size);
      console.log(productsForFilter);
      $placeToShow.html(productsForFilter.length);
      break;
    }
  }
};

function updateProductQuantityForFilters() {
  $(".filter__checkbox").each(function () {
    showProductQuantityForFilter(products, $(this), $(this).siblings(".filter__number-bg").children(".filter__number"));
  });
};

function updateActiveFilters() {
  //clear activeFilter
  activeFilters.categories = [];
  activeFilters.colors = [];
  activeFilters.sizes = [];
  activeFilters.tags = [];

  //checkboxes
  $(".filter__checkbox").each(function () {
    if ($(this).prop("checked")) {
      let filterId = $(this).attr("id");
      let filterType = $(this).data("filter-type");

      switch (filterType) {
        case "category": {
          activeFilters.categories.push(filterId);
          break;
        };
        case "color": {
          activeFilters.colors.push(filterId);
          break;
        };
        case "size": {
          activeFilters.sizes.push(filterId);
          break;
        };
      }
    }
  });

  //tags
  $('.tag[data-tag-active="true"]').each(function () {
    activeFilters.tags.push($(this).attr("id"));
  });

  //price slider
  activeFilters.priceMin = parseInt($(".filter-price-min-input").val());
  activeFilters.priceMax = parseInt($(".filter-price-max-input").val());

  console.log("activeFilters", activeFilters);
};

function filterProducts(productsArray, { categories, colors, sizes, tags, priceMin, priceMax }) {
  //отфильтруй массив продуктов
  //в которых есть переданные параметры
  //и перерисуй сетку продуктов
  filteredProducts = productsArray;

  if (categories.length) {
    filteredProducts = filteredProducts.filter(product => {
      return categories.some(category => category == product.category)
    });
  };

  if (colors.length) {
    filteredProducts = filteredProducts.filter(product => {
      return colors.some(color => color == product.color)
    })
  };

  if (sizes.length) {
    filteredProducts = filteredProducts.filter(product => {
      return sizes.some(size => size == product.size)
    })
  };

  if (tags.length) {
    filteredProducts = filteredProducts.filter(product => {
      return tags.some(tag => product.tags.some(productTag => {
        console.log("heee", tag, productTag);
        return tag == productTag
      }))
    })
  };

  filteredProducts = filteredProducts.filter(product => product.price >= priceMin && product.price <= priceMax);

  console.log("filteredProducts", filteredProducts)
  displayProducts(document.querySelector("#product-list-container"), filteredProducts);
}

$(".tag--filter").click(function() {
  $(this).toggleClass("tag-selected");
});

