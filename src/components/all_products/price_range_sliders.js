const priceSliderModal = $("#price-range-slider1").slider({
  min: 135,
  max: 10000,
  step: 100,
  tooltip: 'always',
  tooltip_position: 'top',
  range: true,
});

const priceSlider = $("#price-range-slider2").slider({
  min: 135,
  max: 10000,
  step: 100,
  tooltip: 'always',
  tooltip_position: 'top',
  range: true,
});

priceSlider.slider("setValue", [activeFilters.priceMin, activeFilters.priceMax]);

function updateShowedPriceRange() {
  $(".filter-price-min-input").val(`${priceSlider.slider('getValue')[0]}`);
  $(".filter-price-max-input").val(`${priceSlider.slider('getValue')[1]}`);
};

updateShowedPriceRange();

priceSlider.on("slide", () => {
  updateShowedPriceRange();
});

$(".btn-filter-price").click(()=> {
  updateActiveFilters();
  priceSlider.slider("setValue", [activeFilters.priceMin, activeFilters.priceMax]);  
  filterProducts(products, activeFilters);
});


