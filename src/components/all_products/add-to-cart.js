document.body.addEventListener("click", (e) => {
  if (e.target.closest(".add-to-cart")) {
    e.preventDefault();
    let productId = e.target.closest(".card").getAttribute("data-product-id");
    cart.addProduct(productId);
    
    e.target.closest(".add-to-cart").children[1].innerHTML = "In cart";
    e.target.closest(".card").firstElementChild.classList.remove("hidden");

    
  }
});



