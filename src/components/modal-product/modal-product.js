$("body").click((e) => {
  if (e.target.classList.contains("card-product__quick-view-btn")) {
    let productId = e.target.closest(".card").getAttribute("data-product-id");
    let product = products.find(obj => obj.id == productId);
    let productDetailsHTML = buildProductDetailsHTML(product);
    function buildProductDetailsHTML(product) {
      return `
      <div class="row" data-product-id = "${product.id}">
          <div class="col-12 col-md-6 col-xl-4 d-flex justify-content-center">
            <div class="product-details__img-container d-flex justify-content-center">
              <img class="product-details__img" src="${product.imgUrl}" alt="chair">
            </div>
          </div>
          <div class="col-12 col-md-6 col-xl-8 ">
            <h3 class="product-details__title">${product.name}</h3>
            <div class="stars d-inline-block">
              <i class="${product.stars >= 1 ? "fas" : "far"} fa-star"></i>
              <i class="${product.stars >= 2 ? "fas" : "far"} fa-star"></i>
              <i class="${product.stars >= 3 ? "fas" : "far"} fa-star"></i>
              <i class="${product.stars >= 4 ? "fas" : "far"} fa-star"></i>
              <i class="${product.stars >= 5 ? "fas" : "far"} fa-star"></i>
            </div>
            <span class="product-details__reviews-num pl-2">(${product.reviews.length} reviews)</span>
            <div class="product-details__price-container py-3 my-3">
              <span class="product-details__price p-2 product-details__price--old ">${product.oldPrice}</span>
              <span class="product-details__price p-2">${product.price}</span>
            </div>
            <div class="product-details__btns d-flex">
              <button class="btn btn-primary mr-2 add-to-cart-modal ${cart.products.some(cartProduct => cartProduct.id == product.id) ? "added-to-cart-modal": ''}" href="#">
                <i class="fas fa-shopping-basket"></i>
                ${cart.products.some(cartProduct => cartProduct.id == product.id) ? "<span>In cart</span>" : "<span>Add to cart</span>"}                
              </button>
              <button class="card-product__icon-button">
                <i class="far fa-heart"></i>
              </button>
            </div>
            <div class="product-details__overview my-3">
              <p class="product-details__title">Quick Review</p>
              <p class="product-details__description mb-2">${product.description}</p>
              <div class="property">
                <span class="product-details__property__title">Availability:</span>
                <span class="product-details__property__value">In stock</span>
              </div>
            </div>
          </div>
          <div class="col-12 ">
            <div class="product-details__btns-social-container pt-3 mt-3 d-flex justify-content-center flex-wrap">
              <button class="btn btn-social m-1">
                <i class="fas fab fa-facebook"></i>
                <span>Share</span>
              </button>
              <button class="btn btn-social m-1">
                <i class="fas fab fa-instagram"></i>
                <span>Share</span>
              </button>
              <button class="btn btn-social m-1">
                <i class="fas fab fa-google"></i>
                <span>Share</span>
              </button>
              <button class="btn btn-social m-1">
                <i class="fas fab fa-pinterest"></i>
                <span>Share</span>
              </button>
            </div>
          </div>
        </div>`;
    };
    $("#product-details-container")
      .html("")
      .append(productDetailsHTML)
  }
});

document.body.addEventListener("click", (e) => {
  if (e.target.closest(".add-to-cart-modal")) {
    let productId = e.target.closest("div[data-product-id]").getAttribute("data-product-id");
    cart.addProduct(productId);

    e.target.closest(".add-to-cart-modal").classList.add("added-to-cart-modal");
    e.target.closest(".add-to-cart-modal").children[1].innerHTML = "In cart";

    displayProducts(document.querySelector("#product-list-container"), filteredProducts);
  }
});
