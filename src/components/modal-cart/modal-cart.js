$(".open-cart-btn").click(() => {
    $("#cart-product-list").html("");
    for (let product of cart.products) {
        $("#cart-product-list").prepend(buildProductInCartHTML(product));
    };
});

//Listen click on + and - quantity buttons and change cart.quantyties
$(window).click(function (e) {
    if ($(e.target).hasClass("cart-product__icon-button-plus")) {
        let productId = $(e.target).closest(".row").data("product-id");
        cart.quantities
            .find(obj => productId == obj.id)
            .quantity++;

        $(e.target)
            .closest(".row")
            .find(".cart-number-of-items")
            .html(cart.quantities.find(obj => productId == obj.id).quantity)
    };

    if ($(e.target).hasClass("cart-product__icon-button-minus")) {
        let productId = $(e.target).closest(".row").data("product-id");
        cart.quantities
            .find(obj => productId == obj.id)
            .quantity--;

        $(e.target)
            .closest(".row")
            .find(".cart-number-of-items")
            .html(cart.quantities.find(obj => productId == obj.id).quantity)
    };

});

function buildProductInCartHTML(product) {
    return `
                      <div class="row border rounded cart-inner-content align-items-center mb-3" data-product-id = "${product.id}">
                        <div class="col-6 col-md-3 col-xl-3">
                            <div class="preview-cart-img">
                                <img src="${product.imgUrl}" alt="product" class="cart-img">
                            </div>
                        </div>

                        <div class="col-6 col-md-9 col-xl-3">
                            <p class="mb-2 fs-22">${product.name}</p>
                            <div class="price"> <span class="cart-thin-text"><s>${product.price}</s></span> <span
                                    class="cart-main-price p-1 ">${product.price}</span></div>
                        </div>

                        <div class="col-6 col-md-9 col-xl-3 d-flex flex-wrap justify-content-end align-items-center">
                            <div class="cart-quantity">
                                <span class="fs-14 pt-2 mr-4">Quantity:</span>
                            </div>
                            <div class="cart-number d-flex">
                                <div class="cart-number-of-items pt-1 pl-4 pr-4">${cart.quantities.find(productInCart => productInCart.id == product.id).quantity}</div>
                                <button class="cart-product__icon-button cart-product__icon-button-minus ">-</button>
                                <button class="cart-product__icon-button cart-product__icon-button-plus">+</button>
                            </div>

                        </div>

                        <div class="col-6 col-md-3 сol-xl-3 text-center">

                            <p class="cart-thin-text">Sum</p>
                            <p class="fs-22 font-weight-bold">$250.00</p>
                        </div>
                    </div>`;
};